# asm startup file
# very first module in load

.text
.globl _ustart, _tutor

_ustart:
	movl $0x3ffff0, %esp   # set user program stack
	movl $0, %ebp          # make debugger backtrace terminate here
 	call _startupc         # call C startup, which calls user main
        #jmp _tutor              # jump to tutor exit

_tutor:
 	int $3                 # return to Tutor

