/*********************************************************************
*
*       file:           testio.c
*       author:         betty o'neil
*
*       test program (application code) for 
*       the device-independent i/o package for SAPC
*
*/

#include <stdio.h>              /* for lib's device # defs, protos */
#include "io_public.h"          /* for our packages devs, API prototypes */
#include <cpu.h>
#include <gates.h>
#include "tsystm.h"


#define MILLION 1000000
#define DELAYLOOPCOUNT (400 * MILLION)
#define BUFLEN 80

extern void syscall(void);
extern void ustart(void);
extern void locate_idt(unsigned int* limitp, char** idtp);
void set_trap_gate(int n, IntHandler *inthand_addr);
void syscallc(int user_eax, int arg1, char* arg2, int arg3);
void initialize(void);


/* Note that kprintf is supplied with the SAPC support library.  It does 
  output polling to the console device with interrupts (temporarily) off.
  We are using it as a debugging tool while working with a development
  system, especially one using interrupts.  */

// modified from cpu.c in $pclibsrc
void set_trap_gate(int n, IntHandler *inthand_addr) {
	char *idt_LAaddr;		/* linear address of IDT */
	char *idt_VAaddr;		/* virtual address of IDT */
	Gate_descriptor *idt, *desc;
	unsigned int limit = 0;

	locate_idt(&limit,&idt_LAaddr);
	/* convert to virtual address, i.e., ordinary address */
	idt_VAaddr = idt_LAaddr - KERNEL_BASE_LA;  /* usable address of IDT */
	/* convert to a typed pointer, for an array of Gate_descriptor */
	idt = (Gate_descriptor *)idt_VAaddr;
	desc = &idt[n];		/* select nth descriptor in idt table */
	/* fill in descriptor */
	desc->selector = KERNEL_CS;	/* CS seg selector for int. handler */
	desc->addr_hi = ((unsigned int)inthand_addr)>>16; /* CS seg offset of inthand  */
	desc->addr_lo = ((unsigned int)inthand_addr)&0xffff;
	desc->flags = GATE_P|GATE_DPL_KERNEL|GATE_TRAPGATE; /* valid, trap */
	desc->zero = 0;
}


// C handler for system calls
void syscallc(int user_eax, int arg1, char* arg2, int arg3) {
	// do syscall depending on eax value
	if ( user_eax == 3 ) {
		user_eax = sysread(arg1, arg2, arg3);
	} else if ( user_eax == 4 ) {
		user_eax = syswrite(arg1, arg2, arg3);
	} else if ( user_eax == 5 ) {
		user_eax = tutor();
	} else {
		// invalid system call
	}
}

void initialize()
{
	// call ioinit()
	ioinit();

	// set trap vector (would this be how you go about it?)
	set_trap_gate(0x80, &syscall);

	ustart();
}

