# asm startup file
# very first module in load
.text
.globl _initkernel, _tutorexit

_initkernel:
   movl $0x3ffff0, %esp   # set kernel stack (same as user stack location for hw2)
   movl $0, %ebp          # make debugger backtrace terminate here
   #call _startupc         # call C startup, which calls user main

_tutorexit:
   int $3                 # return to Tutor

