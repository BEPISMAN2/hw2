.text
.globl _syscall, _locate__idt

// from cpureg.s
_locate__idt:
	sidt idt_info		# SIDT instruction: store IDT
	movl 4(%esp), %ecx	# pointer to place for limit
	movw idt_limit, %dx	# return limit to caller
	movw %dx, (%ecx)
	movl 8(%esp), %ecx	# pointer to place for addr
	#movl idraddr, %edx	# return addr to caller
	#movl %edx, (%ecx)	
	ret

// from notes
_syscall:
	# push edx -> eax to stack
	pushl %edx
	pushl %ecx
	pushl %ebx
	pushl %eax

	# call c trap routine located in tunix.c (presumably)
	call _syscallc

	# pop values of eax -> edx from stack
	popl %eax
	popl %ebx
	popl %ecx
	popl %edx
	iret

.data
idt_info:
idt_limit:	.word 1
idt_addr:	.long 1
gdt_info:
gdt_limit:	.word 1
gdt_addr:	.long 1

